//
//  ContentView.swift
//  IamRich-SwiftUI
//
//  Created by Cohen, Dor on 18/10/2020.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        ZStack{
            Color(.cyan).edgesIgnoringSafeArea(/*@START_MENU_TOKEN@*/.all/*@END_MENU_TOKEN@*/)
            VStack {
                Text("I Am Rich")
                    .fontWeight(.bold)
                    .padding()
                    .foregroundColor(/*@START_MENU_TOKEN@*/.blue/*@END_MENU_TOKEN@*/)
                    .font(.system(size: 90))
                Image(systemName: "dot.radiowaves.left.and.right").resizable()
                    .aspectRatio(contentMode: .fit)
                    .frame(width: 200.0, height: 200.0)
            }
        }
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView().previewDevice(PreviewDevice(rawValue: "iPhone Xs Max"))
    }
}
