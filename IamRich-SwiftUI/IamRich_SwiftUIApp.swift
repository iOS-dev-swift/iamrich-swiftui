//
//  IamRich_SwiftUIApp.swift
//  IamRich-SwiftUI
//
//  Created by Cohen, Dor on 18/10/2020.
//

import SwiftUI

@main
struct IamRich_SwiftUIApp: App {
    var body: some Scene {
        WindowGroup {
            ContentView()
        }
    }
}
